var express = require('express');
const db = require('../db.js')
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {
  const sql ='INSERT INTO orders (products, price) VALUES (?,?)'
  const params = [req.body.products.join(','), req.body.price]
  db.run(sql, params, function (err, result) {
    if (err){
      res.status(400).json({"error": err.message})
      return;
    }
    res.status(201).end()
  });
});

router.get('/', function(req, res, next) {
  const sql = 'SELECT id, purchase_time, price from orders'
  db.all(sql, (err, rows) => {
    if (err) {
      res.status(400).json({"error":err.message});
      return;
    }
    res.json(rows)
  })
});
module.exports = router;
