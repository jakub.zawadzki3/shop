var express = require('express');
const db = require('../db.js')
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  const sql = 'SELECT DISTINCT category from products'
  db.all(sql, (err, rows) => {
    if (err) {
      res.status(400).json({"error":err.message});
      return;
    }
    res.json(rows.map(row => row.category))
  })
});

module.exports = router;
