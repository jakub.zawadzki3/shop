var express = require('express');
var router = express.Router();
let db = require('../db.js')

/* GET users listing. */
router.get('/', function(req, res, next) {
  let category = req.query.category
  if (!category) {
    category = '%'
  }
  const sql = 'SELECT * from products where category LIKE ?'
  db.all(sql, [category], (err, rows) => {
    if (err) {
      res.status(400).json({"error":err.message});
      return;
    }
    res.json(rows)
  })
});

module.exports = router;
