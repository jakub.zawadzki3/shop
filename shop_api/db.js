var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = 'db.sqlite'

let db = new sqlite3.Database(DBSOURCE, (err) => {
  if (err) {
    // Cannot open database
    console.error(err.message)
    throw err
  } else {
    console.log('Connected to the SQLite database.')
    db.run(`CREATE TABLE products
            (
                id       INTEGER PRIMARY KEY AUTOINCREMENT,
                name     text UNIQUE,
                category text,
                price    number
            )`,
      (err) => {
        if (err) {
          // Table already created
          console.log(err)
        } else {
          // Table just created, creating some rows
          var insert = 'INSERT INTO products (name, category, price) VALUES (?,?,?)'
          db.run(insert, ['cytryna', 'owoc', 1.5])
          db.run(insert, ['banan', 'owoc', 3])
          db.run(insert, ['pomidor', 'warzywo', 4])
          db.run(insert, ['kurczak', 'mięso', 11])
          db.run(insert, ['wołowina', 'mięso', 13])
          db.run(insert, ['wieprzowina', 'mięso', 9])
          db.run(insert, ['lodówka', 'AGD', 1850])
          console.log('Table initialized.')
        }
      })

    db.run(`CREATE TABLE orders
            (
                id    INTEGER PRIMARY KEY AUTOINCREMENT,
                products text,
                price number,
                purchase_time DATETIME DEFAULT CURRENT_TIMESTAMP
            )`,
      (err) => {
        if (err) {
          console.log(err)
        }
      })
  }
})

module.exports = db
